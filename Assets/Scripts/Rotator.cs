using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField][Range(0, 1)] float xRotationUp, yRotationUp, zRotationUp;
    [SerializeField][Range(0, 1)] float xRotationForward, yRotationForward, zRotationForward;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LookRotation()
    {
        Debug.DrawRay(transform.position, new Vector3(xRotationUp, yRotationUp, zRotationUp) * 30, Color.green, 10);
        Debug.DrawRay(transform.position, new Vector3(xRotationForward, yRotationForward, zRotationForward) * 30, Color.blue, 10);
        transform.rotation = Quaternion.LookRotation(new Vector3(xRotationForward, yRotationForward, zRotationForward), new Vector3(xRotationUp,yRotationUp,zRotationUp));
       
    }
    public void FromRotation()
    {
       
    }
}
