using UnityEditor;
using UnityEngine;

public class SceneViewFollow : MonoBehaviour
{
    public Transform target;

    void LateUpdate()
    {
#if UNITY_EDITOR
        if (target != null)
        {
            SceneView sceneView = SceneView.lastActiveSceneView;
            if (sceneView != null)
            {
                sceneView.pivot = target.position;
                sceneView.Repaint();
            }
        }
#endif
    }
}