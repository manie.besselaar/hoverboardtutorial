using System;
using UnityEngine;

public class Hoverboard : MonoBehaviour
{
    [SerializeField] float _targetHoverHeight = 2.5f; 
    [SerializeField] float _hoverForce = 4.0f;
    [SerializeField] float _damping = 0.1f;
    [SerializeField][Tooltip("How far ahead to look for elevation changes in your direction of travel.")] float _velocityLookAhead = 0.5f;
    [SerializeField][Tooltip("Rate at which the board can tilt in response to the terrain.")] float _rotationRate = 15f;
    [SerializeField][Tooltip("Height above the board where raycasting should originate for terrain detection.")] float _raycastHeightAboveBoard = 3;
   
    [SerializeField][Tooltip("How much the board leans forward or backward with the Vertical input.")] float _leanBack =.2f;
    float _leanBackAmount = 0;//used to track the lean ammount as influenced by the current vertical input axis value
    [SerializeField][Tooltip("How much the board leans left or right with the horizontal input.")] float _leanSide = .2f;
    float _leanSideAmount = 0; //used to track the lean ammount as influenced by the current horizontal input axis value

    //Cache variables to minimise gc
    Transform _transform;
    Rigidbody _rigidBody;
 
    Vector3 _raycastAdVance= Vector3.zero; //This vector stores the vector to by wich to offset the advance of the terrain probe
    
    Vector3 _currentForward;//stores the current forward vector of the board
    Vector3 _upVector; //Stores the current up vector for the board
    Vector3 _forward; //Stores the new forward vector after taking the current _leanBackAmmount value into account as well as the terrain elevation
    float _error; //Difference between the board's current height and the target height
    Vector3 _force; // The force vector to apply to lift the board
    Vector3 _velocity; // Used to store the velocity vector to accomodate velocity damping
  [SerializeField] LayerMask _groundMask;

    void Start()
    {
        _transform = transform;
        _rigidBody = GetComponent<Rigidbody>();
     //   _hits = 
      //  TestDot();
    }
   
    void FixedUpdate()
    {
          RaycastHit[] _hits_height = new RaycastHit[10]; 
        //Calculate how far ahead and above the board the first raycast position should be to help anticipate terrain changes
        _raycastAdVance = _rigidBody.transform.up * _raycastHeightAboveBoard + _rigidBody.velocity * _velocityLookAhead;

        // Calculate the amount of force we need to apply to hover for the advance position raycast.
        //var hits = Physics.RaycastNonAlloc(_transform.position + _raycastAdVance, -_transform.up, _hits_height, _targetHoverHeight + _raycastAdVance.y);

        var hits = Physics.RaycastNonAlloc(_transform.position + _raycastAdVance, -_transform.up, _hits_height, _targetHoverHeight + _raycastAdVance.y,_groundMask);
        if (hits > 0)
        {
             _error = _targetHoverHeight - (_hits_height[0].distance - _raycastHeightAboveBoard) ;
             _force = transform.up * (_error * _hoverForce);
           
            _rigidBody.AddForce(_force, ForceMode.Acceleration);
        }
        // Calculate the amount of force we need to apply to hover for the transform position raycast.
        //Having the second raycast helps prevent the board from bottoming out when cresting a rise
        //TODO: Elimintate this repitition, perhaps by tuning the magnitude of the advance of the first raycast
        hits = (Physics.RaycastNonAlloc(_transform.position + _rigidBody.transform.up * _raycastHeightAboveBoard, -_transform.up, _hits_height, _targetHoverHeight +  _raycastHeightAboveBoard,_groundMask ));
        if (hits > 0)
        {
            _error = _targetHoverHeight - (_hits_height[0].distance - _raycastHeightAboveBoard);
            _force = transform.up * (_error * _hoverForce);
            
            _rigidBody.AddForce(_force, ForceMode.Acceleration);
        }

        // Smooth it out with damping
        _velocity = _rigidBody.velocity;
        _velocity *= (1.0f - _damping * Time.deltaTime);
        _rigidBody.velocity = _velocity;
        //   MonitorAttitude();
    //Get the inputs for the lean angles   
    SetLeanAngles();
//Adjust the attitude of the board to match the terrain
    AttitudeAdjust();
    }
   
    /// <summary>
    /// This function adjusts the angle of the hoverboard to try and keep it close to parralel to the terrain, while still taking into account the lean inputs made by the player.
    /// </summary>
    private void AttitudeAdjust()
    {
    
        _currentForward = _rigidBody.transform.forward.normalized;
         _upVector = Vector3.up; // set this to up in case there is no raycast hit, the board will return to the upright position when too high off the ground 
     RaycastHit[] _hits =  new RaycastHit[10];

        //Cast down from a point above and to the front of the board to set the board pitch
        var hits = (Physics.RaycastNonAlloc(_transform.position + _raycastAdVance,

            -_transform.up, _hits, _targetHoverHeight + 4));  //TODO: Can we roll this directly into the if statement to avoid allocating another variable(int)?
        if (hits > 0)
        {

            _upVector = _hits[0].normal;
         }
      //  Debug.Log("UpVector " + _upVector + " currentForward " + _currentForward + " dot product " + Vector3.Dot(_currentForward, _upVector).ToString());

        _forward = _currentForward - _upVector * Vector3.Dot(_currentForward, _upVector) +
            (transform.up * _leanBackAmount); //TODO: Explain this in 2d drawing!!
      //  Debug.Log("_Forward " + _forward + " normalised " + _forward);
        //Do rotational lerp towards the desired rotation
        _rigidBody.rotation = Quaternion.Slerp(_rigidBody.rotation, Quaternion.LookRotation(_forward.normalized, _upVector + transform.right * _leanSideAmount), Time.fixedDeltaTime * _rotationRate);

    }
    void SetLeanAngles()
    {
        //TODO:This could be better with the new input system
        //TODO: Try controlling camera with mouse and lean left and right with button 1 and 2 while using arrows or wasd for turn and thrust.
        // maby ad spacebar to jump.
        //Should I seperate out my forward and backward lean controls too? Will this get too involved for the player?
        //Get the input for the lean angles
        if (Input.GetKey(KeyCode.T)) 
        {
            _leanBackAmount = _leanBack;
        }
        else if ((Input.GetKey(KeyCode.G)))
        {
            _leanBackAmount = - _leanBack;
        }
        else
        {
            _leanBackAmount = 0; //Reset _leanBack to neutral if no input is made
        }
       if(Input.GetKey(KeyCode.F)) {
            _leanSideAmount = -_leanSide;

        }else if (Input.GetKey(KeyCode.H)) {

            _leanSideAmount = _leanSide;
        }
        else
        {
            _leanSideAmount = 0;
        }

       if (Input.GetKeyDown(KeyCode.Space)) {

            _rigidBody.AddForce(transform.up * 100);
                }
    }
    private void OnDrawGizmos()
    {
        //Draw the gizmo to show where the scanning probe is.
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + _raycastAdVance,0.1f);
    }
    void TestDot()
    {
        Vector2 v1 = new Vector2(2, -1);
        Vector2 v2 = new Vector2(-1, -1);
        Vector2 v3 = new Vector2(-1,1);
        float value = Vector2.Dot(Vector2.right, v1);
        Debug.Log("V1 and right " +value );

        Debug.Log("v1 and down " + Vector2.Dot(-Vector2.up,v1));
        Debug.Log("v1 and up " + Vector2.Dot(Vector2.up, v1));
        Debug.Log("V1 and left " + Vector2.Dot(-Vector2.right, v1));
        Debug.Log("Cross " + Vector3.Cross(new Vector3(4, 1, 0), new Vector3(0, 0, 1)));
    }
}
