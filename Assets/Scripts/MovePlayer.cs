using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    [SerializeField] float _moveSpeed = 5f, _turnSpeed = 2.0f;
    
    float _turnAmount;
    float _forwardAmount;
    Rigidbody _rigidBody;
    Transform _transform;

    void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _transform = transform;
    }

    void Update()
    {
        HandlePlayerInput();
    }

    void FixedUpdate()
    {
        HandleMovement();
    }

    void HandlePlayerInput()
    {
        _turnAmount = Input.GetAxis("Horizontal");
        _forwardAmount = Input.GetAxis("Vertical");
    }

    void HandleMovement()
    {
        if (!Mathf.Approximately(0f, _turnAmount))
        {
            _rigidBody.AddTorque(Vector3.up * (_turnAmount * _turnSpeed * Time.fixedDeltaTime));
        }

        if (!Mathf.Approximately(0f, _forwardAmount))
        {
            _rigidBody.AddForce(_transform.forward * (_forwardAmount * _moveSpeed * Time.fixedDeltaTime));
        }
    }
 
}
