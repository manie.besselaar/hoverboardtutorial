using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalCaster : MonoBehaviour
{
    RaycastHit hit;
    Vector3 _hitPoint = Vector3.zero;
    // Update is called once per frame
    void Update()
    {
        Physics.Raycast(transform.position, Vector3.down,out hit, 10);

        if(hit.collider != null)
        {
            _hitPoint = hit.point;
            Debug.DrawRay(_hitPoint, hit.normal * 20, Color.red,1f);
        }
       

    }
}
